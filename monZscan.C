#include <TFile.h>
#include <TGraph.h>
#include <TCanvas.h>
#include <TPad.h>
#include <TAxis.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TLine.h>
#include <TMultiGraph.h>
#include "TString.h"
#include <string.h>
#include <iostream>
#include <stdio.h>
#include <math.h>
#include <sstream>
#include <fstream>
#include <vector>

using namespace std;

void monZscan(string dataFileName, Int_t mainHarm=2){
   string macroComm;

  ostringstream ssMainHarm;
  ssMainHarm << mainHarm;

  while(1){

    macroComm="harmAna(\""+dataFileName+"\")";
     cout << macroComm << endl;
     gROOT->ProcessLine(macroComm.c_str());

     macroComm="anaZscan(\""+dataFileName+"\","+ssMainHarm.str()+")";
     cout << macroComm << endl;
     gROOT->ProcessLine(macroComm.c_str());

  }
}
